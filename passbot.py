#!/usr/bin/env python3

from sys import stdin
from simplecrypt import encrypt,decrypt
from getpass import getpass
import yaml
from os import system,path
import bcrypt
from clmenu import getch,Menu,printLogo

def save(key,value):
    pair={}
    passcode=getpass("\n\t\t\t\t\t\tEnter a key to encrypt the password: ")
    print("\n\n\t\t\t\t\t\tWrite this down somewhere safe: "+passcode)
    value=encrypt(passcode,value)
    pair[key]=value
    file=open("passwords.yaml","a")
    yaml.dump(pair,file)
    print("\n\t\t\t\t\t\t\tSaved successfully.")

def login():
    if(path.exists("passwords.yaml")):
        with open("passwords.yaml","r") as file:
            data=file.read()
            data=yaml.load(data)
        hashed_password=data['main_pass']
    #hashed_password=b'$2b$12$ifJYzVz1yns0pNuw6GPa0esJFHcxYRqOVy33jZ6P4wTYxjuC4XMFa'
        count=0
        while count<=2:
            printLogo("logo.txt")
            print("\n\n")
            password=getpass("\t\t\t\t\t\tEnter the password to go forward: ")
            if(bcrypt.checkpw(password.encode('utf-8'),hashed_password)):
                return True
            count+=1
        return False
    else:
        printLogo("logo.txt")
        print("\n\n")
        passw=getpass("\t\t\t\t\tEnter a password (you will need this to login every time you run the script) :")
        file=open("passwords.yaml","a")
        yaml.dump({'main_pass':passw},file)
        login()

def view():
    printLogo("logo.txt")
    platform=input("\n\t\t\t\t\t\tEnter the name of the platform: ")
    get=getch()
    try:    
        file=open("passwords.yaml","r")
        data=file.read()
        platforms=yaml.load(data)
    except:
        print("There are no passwords to view. Ty using the add menu.")
        return False
    if(not platforms):
        print("\t\t\t\t\t\tThere are no password to view.")
        get()
        return False
    if platform in platforms.keys():
        count=0
        while count<5:
            printLogo("logo.txt")
            passcode=getpass("\t\t\t\t\t\tEnter the key you used to encrypt: ")
            try:
                encrypted=platforms[platform]
                password=decrypt(passcode,encrypted)
                print("\n\t\t\t\t\tPassword for {} is {}.".format(platform,password))
                get()
                return True
            except:
                count+=1
                print("\t\t\t\t\t\tIncorrect key. Decryption failed. Tries left: {}".format(5-count))

        return False

def newPlatform():
    printLogo("logo.txt")
    print("\n\n")
    print("\t\t\t\t\t\tEnter the name of the platform: ",end=" ")
    name=input()
    while True:
        password=getpass("\n\t\t\t\t\t\tEnter the password for the platform: ")
        print("\n\t\t\t\t\t\tRe-enter the password: ",end=" ")
        repassword=getpass()
        if(password==repassword):
            break
        print("\n\t\t\t\t\t\tPasswords do not match. Try again:")
    kvpair={name:password}
    save(name,password)

def main():
    if(login()):
        options=["ADD  PLATFORM","VIEW PASSWORD","    EXIT     "]
        instructs="USE THE ARROW KEY TO NAVIGATE"
        menu=Menu(options,instructs,"logo.txt")
        temp=9
        while(temp!=2):
            temp=menu.prompt()
            if(temp==0):
                newPlatform()
            elif(temp==1):
                view()
    else:
        print("\n\t\t\t\t\t\t\tLogin failed.")

if __name__=="__main__":
    main()
